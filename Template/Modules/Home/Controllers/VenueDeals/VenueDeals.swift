//
//  VenueDeals.swift
//  Template
//
//  Created by Amaxza Digital Inc 1 on 08/11/2018.
//  Copyright © 2018 Amaxza Digital Inc. All rights reserved.
//

import UIKit

class VenueDeals: UIViewController {

    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tfSearchField: UITextField!
    var arrDealsCount = 4
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnSearch(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.tfSearchField.isHidden = !sender.isSelected
        if sender.isSelected{
            self.lblHeading.text = ""
            self.tfSearchField.becomeFirstResponder()
        }
        else{
            self.lblHeading.text = Strings.DUMMY_VENUE.text
            self.tfSearchField.resignFirstResponder()
        }
    }
    @IBAction func onBtnSettings(_ sender: UIButton) {
    }
}
extension VenueDeals{
    private func pushToBoothMenuScreen(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "BoothMenuScreen")
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
extension VenueDeals:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDealsCount
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VenueDealsTVC", for: indexPath) as! VenueDealsTVC
        cell.setData()
        return cell
    }
}
extension VenueDeals:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.height * 0.4
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushToBoothMenuScreen()
    }
}
extension VenueDeals:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tfSearchField.isHidden = true
        self.lblHeading.text = Strings.DUMMY_VENUE.text
        textField.resignFirstResponder()
        return true
    }
}
