//
//  iCarouselCell.swift
//  Template
//
//  Created by Amaxza Digital Inc 1 on 08/11/2018.
//  Copyright © 2018 Amaxza Digital Inc. All rights reserved.
//

import UIKit

class iCarouselCell: UIView {

    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblDeliveryTime: UILabel!
    @IBOutlet weak var btnOrderNow: UIButton!
    @IBOutlet weak var btnGetDirection: UIButton!
    @IBOutlet weak var cardViewTop: NSLayoutConstraint!
    @IBOutlet weak var cardViewBottom: NSLayoutConstraint!
    
    class func instanceFromNib() -> UIView{
        return UINib(nibName: "iCarouselCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! iCarouselCell
    }
    func setFrame(cardView: iCarouselCell,isCurrent:Bool,parentHeight:CGFloat){
        guard let top = cardView.cardViewTop else {return}
        guard let bottom = cardView.cardViewBottom else {return}
        if isCurrent{
            UIView.animate(withDuration: 0.25) {
                top.constant = 4
                bottom.constant = -4
                self.layoutIfNeeded()
            }
        }
        else{
            UIView.animate(withDuration: 0.25) {
                top.constant = 0.1875 * parentHeight
                bottom.constant = -(0.13 * parentHeight)
                self.layoutIfNeeded()
            }
        }
    }
}
