//
//  VenueDealsOnMap.swift
//  Template
//
//  Created by Amaxza Digital Inc 1 on 08/11/2018.
//  Copyright © 2018 Amaxza Digital Inc. All rights reserved.
//

import UIKit
import GoogleMaps

class VenueDealsOnMap: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var view_iCarousel: iCarousel!
    @IBOutlet weak var tfSearchField: UITextField!
    @IBOutlet weak var lblHeading: UILabel!
    var panGesture = UIPanGestureRecognizer()
    var arrMenuCount = 10
    var arr_iCarouselItems = [iCarouselCell]()
    var currentView = iCarouselCell()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for _ in 0..<self.arrMenuCount{
            self.arr_iCarouselItems.append(iCarouselCell())
        }
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.prepareDataSetForICarouselCustomization()
    }

    @IBAction func onBtnBack(_ sender: UIButton) {
        self.view_iCarousel.alpha = 0.0
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnSearch(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.tfSearchField.isHidden = !sender.isSelected
        if sender.isSelected{
            self.lblHeading.text = ""
            self.tfSearchField.becomeFirstResponder()
        }
        else{
            self.lblHeading.text = Strings.DUMMY_VENUE.text
            self.tfSearchField.resignFirstResponder()
        }
    }
    @IBAction func onBtnSettings(_ sender: UIButton) {
    }
}
//MARK:- Helper Methods
extension VenueDealsOnMap{
    private func prepareDataSetForICarouselCustomization(){
        self.view_iCarousel.type = .invertedCylinder
        self.view_iCarousel.dataSource = self
        self.view_iCarousel.delegate = self
        self.view_iCarousel.scrollToItem(at: self.arrMenuCount - 1, animated: true)
        self.view_iCarousel.scrollToItem(at: 1, animated: true)
        self.panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.draggedView(_:)))
        self.view_iCarousel.scrollSpeed = 0.5
    }
    @objc func draggedView(_ sender:UIPanGestureRecognizer){
        self.view.bringSubview(toFront: currentView)
        let translation = sender.translation(in: self.view)
        self.currentView.center = CGPoint(x: (self.currentView.center.x) + translation.x, y: (self.currentView.center.y) + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
    private func pushToVenueDeals(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "VenueDeals")
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
extension VenueDealsOnMap: iCarouselDataSource{
    func numberOfItems(in carousel: iCarousel) -> Int {
        return self.arrMenuCount
    }
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let cell = iCarouselCell.instanceFromNib() as! iCarouselCell
        cell.frame.size.height = self.view_iCarousel.frame.height * 0.98
        cell.frame.size.width = self.view_iCarousel.frame.width * 0.76
        self.arr_iCarouselItems[index] = cell
        return cell
    }
}
extension VenueDealsOnMap: iCarouselDelegate{
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        return value * 1.01
    }
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        let index = carousel.currentItemIndex
        let parentHeight = self.view_iCarousel.frame.height
        for (i,obj) in arr_iCarouselItems.enumerated(){
            if i == index{
                obj.setFrame(cardView: obj, isCurrent: true, parentHeight:parentHeight)
                self.currentView = obj
                //self.currentView.addGestureRecognizer(panGesture)
            }
            else{
                obj.setFrame(cardView: obj, isCurrent: false, parentHeight:parentHeight)
            }
        }
    }
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        self.pushToVenueDeals()
    }
}
extension VenueDealsOnMap:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tfSearchField.isHidden = true
        self.lblHeading.text = Strings.DUMMY_VENUE.text
        textField.resignFirstResponder()
        return true
    }
}
