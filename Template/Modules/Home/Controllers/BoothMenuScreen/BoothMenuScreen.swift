//
//  BoothMenuScreen.swift
//  Template
//
//  Created by Amaxza Digital Inc 1 on 09/11/2018.
//  Copyright © 2018 Amaxza Digital Inc. All rights reserved.
//

import UIKit

class BoothMenuScreen: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var navBar: UIView!
    var coverImageHeight:CGFloat = 0.0
    var arrDealsCount = 6
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.coverImageHeight = self.imgCover.frame.height
        self.tableView.contentInset = UIEdgeInsetsMake(self.coverImageHeight, 0, 0, 0)
        self.view.layoutIfNeeded()
        self.imgCover.contentMode = .scaleAspectFill
        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension BoothMenuScreen: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDealsCount
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.backgroundColor = .black
        return cell
    }
}
extension BoothMenuScreen:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.height * 0.4
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = self.coverImageHeight - (scrollView.contentOffset.y + self.coverImageHeight)
        let height = min(max(y, 60), 400)
        self.imgCover.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
        if Int(height) == 60{
            self.navBar.backgroundColor = ColorConstants.APP_COLOR
        }
        else{
            self.navBar.backgroundColor = .clear
        }
    }
}
