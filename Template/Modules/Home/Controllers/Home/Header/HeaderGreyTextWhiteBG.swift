//
//  HeaderGreyTextWhiteBG.swift
//  Template
//
//  Created by Amaxza Digital Inc 1 on 06/11/2018.
//  Copyright © 2018 Amaxza Digital Inc. All rights reserved.
//

import UIKit

class HeaderGreyTextWhiteBG: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    
    class func instanceFromNib() -> UIView{
        return UINib(nibName: "HeaderGreyTextWhiteBG", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! HeaderGreyTextWhiteBG
    }
}
