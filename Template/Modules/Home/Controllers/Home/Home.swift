//
//  Home.swift
//  Template
//
//  Created by Amaxza Digital Inc 1 on 06/11/2018.
//  Copyright © 2018 Amaxza Digital Inc. All rights reserved.
//

import UIKit

class Home: UIViewController {

    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var tfSearchField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnSettings(_ sender: UIButton) {
    }
    @IBAction func onBtnSearch(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.tfSearchField.isHidden = !sender.isSelected
        if sender.isSelected{
            self.lblHeading.text = ""
            self.tfSearchField.becomeFirstResponder()
        }
        else{
            self.lblHeading.text = Strings.DUMMY_VENUE.text
            self.tfSearchField.resignFirstResponder()
        }
    }
}
//MARK:- Helper Methods
extension Home{
    private func pushToVenueDealsOnMap(){
        let storyboard = AppStoryboard.Home.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "VenueDealsOnMap")
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
//MARK:- UITableViewDataSource
extension Home:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return 3
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTVC", for: indexPath) as! HomeTVC
        cell.setData()
        return cell
    }
}
//MARK:- UITableViewDelegate
extension Home:UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = HeaderGreyTextWhiteBG.instanceFromNib() as! HeaderGreyTextWhiteBG
        switch section {
        case 0:
            header.lblTitle.text = "You are near"
            return header
        default:
            header.lblTitle.text = "Other Venues"
            return header
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushToVenueDealsOnMap()
    }
}
extension Home:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tfSearchField.isHidden = true
        self.lblHeading.text = Strings.HOME_HEADING.text
        textField.resignFirstResponder()
        return true
    }
}
