//
//  HomeTVC.swift
//  Template
//
//  Created by Amaxza Digital Inc 1 on 06/11/2018.
//  Copyright © 2018 Amaxza Digital Inc. All rights reserved.
//

import UIKit

class HomeTVC: UITableViewCell {

    @IBOutlet weak var heightConstraintImage: NSLayoutConstraint!
    @IBOutlet weak var imgVenue: UIImageView!
    @IBOutlet weak var lblVenue: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var btnGetDirection: UIButton!
    
    func setData(){
        let screenHeight = Constants.screenHeight
        self.heightConstraintImage.constant = 0.20 * screenHeight
        self.selectionStyle = .none
    }
}
