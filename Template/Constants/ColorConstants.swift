//
//  ColorConstants.swift
//  The Court Lawyer
//
//  Created by M.Usman Bin Rehan on 5/9/18.
//  Copyright © 2018 M.Usman Bin Rehan. All rights reserved.
//

import Foundation
import UIKit
struct ColorConstants {
    static let NAV_BAR_PURPLE = UIColor(hexString: "#423144")
    static let NAV_BAR_RED    = UIColor(hexString: "#DE4248")
    static let APP_COLOR      = UIColor(hexString: "#E65359")
}
extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}
