//
//  Strings.swift
//  Template
//
//  Created by Amaxza Digital Inc 1 on 09/11/2018.
//  Copyright © 2018 Amaxza Digital Inc. All rights reserved.
//

import Foundation
enum Strings:String{
    case ERROR = "Error"
    case INVALID_USER_NAME = "Please enter valid username."
    case INVALID_PASSWORD = "Please enter valid password of atleast 6 characters."
    case HOME_HEADING = "Select Venue"
    case SEARCH_PLACEHOLDER = "Search"
    case DUMMY_VENUE = "Gillete Stadium"
    
    var text: String{return self.rawValue}
}
