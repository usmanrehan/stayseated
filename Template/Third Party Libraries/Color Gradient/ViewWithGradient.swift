//
//  ViewWithGradient.swift
//  Template
//
//  Created by Amaxza Digital Inc 1 on 06/11/2018.
//  Copyright © 2018 Amaxza Digital Inc. All rights reserved.
//

import UIKit

class ViewWithGradient : UIView{
    override func layoutSubviews() {
        super.layoutSubviews()
        var gradientLayer: CAGradientLayer!
        let purpleColor = ColorConstants.NAV_BAR_PURPLE.cgColor
        let redColor = ColorConstants.NAV_BAR_RED.cgColor
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [purpleColor, redColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y: 0.5)
        self.layer.addSublayer(gradientLayer)
    }
}
class ViewWith_W_B_Gradient : UIView{//White -> Black
    override func layoutSubviews() {
        super.layoutSubviews()
        var gradientLayer: CAGradientLayer!
        let black = UIColor.black.cgColor
        let white = UIColor(hexString: "FFFFFF", alpha: 0.0).cgColor
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [white,black]
        self.layer.addSublayer(gradientLayer)
    }
}
class ViewWith_B_W_Gradient : UIView{//Black -> White
    override func layoutSubviews() {
        super.layoutSubviews()
        var gradientLayer: CAGradientLayer!
        let black = UIColor(hexString: "#000000", alpha: 0.50).cgColor
        let white = UIColor(hexString: "#000000", alpha: 0).cgColor
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [black,white]
        self.layer.addSublayer(gradientLayer)
    }
}
class CustomFontOfButton : UIButton{
    override func layoutSubviews() {
        super.layoutSubviews()
        let currentFont:CGFloat = 11.0
        let constantHeight:CGFloat  = 667.0//For Specific Size Class
        let phoneHeight = Constants.screenHeight
        let font = (currentFont/constantHeight) * phoneHeight
        self.titleLabel?.font =  UIFont(name: "Lato", size: font)
    }
}
@objc class NavigationGradient: NSObject{
    static let main = NavigationGradient()
    func setAppGradient(view:UIView)->CAGradientLayer{
        var gradientLayer: CAGradientLayer!
        let purpleColor = ColorConstants.NAV_BAR_PURPLE.cgColor
        let redColor = ColorConstants.NAV_BAR_RED.cgColor
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [purpleColor, redColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y: 0.5)
        return gradientLayer
    }
    func setBWGradient(view:UIView)->CAGradientLayer{
        var gradientLayer: CAGradientLayer!
        let black = UIColor(hexString: "#000000", alpha: 0.50).cgColor
        let white = UIColor(hexString: "#000000", alpha: 0).cgColor
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [black,white]
        return gradientLayer
    }
}
